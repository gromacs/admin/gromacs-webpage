Tutorials and Webinars
=======================

Tutorials
----------

On the `GROMACS tutorial page <http:\\tutorials.gromacs.org>`_  you find a
collection of training resource and free online GROMACS tutorials, provided as
interactive Jupyter notebooks.

Workshops materials
--------------------

:doc:`workshop`: Learn to code in GROMACS (2024)

    * lecture materials `doi:10.5281/zenodo.13739992 <https://zenodo.org/records/13739992>`_

:doc:`workshop`: Learn to code in GROMACS (2023)

    * lecture materials `doi:10.5281/zenodo.10276348 <https://zenodo.org/records/10276348>`_

`Workshop: How to run GROMACS efficiently on the LUMI supercomputer <https://bioexcel.eu/events/running-gromacs-efficiently-on-lumi-workshop-2024/>`_
(2024) 

    * `overview and collaborative document <https://zenodo.org/records/10683366/files/Gromacs-on-LUMI-Q_A-document.pdf?download=1>`_ used during the workshop
    * lecture and hands-on materials `doi:10.5281/zenodo.10683366 <https://doi.org/10.5281/zenodo.10683366>`_

    
GROMACS webinars
-----------------

`GROMACS <https://www.youtube.com/watch?v=MWafKFVgFTU>`_

GROMACS-related webinars
-------------------------

`Improvements in the GROMACS heterogeneous parallelization <https://youtu.be/rTUz28f8p6g>`_

`Getting good performance in GROMACS default <https://youtu.be/RUqdzntAgMc>`_

`Applying the Accelerated Weight Histogram method to alchemical transformations <https://youtu.be/E5nGLcbyqTQ>`_

`NB-LIB: A performance portable library for computing forces and energies of multi-particle systems <https://youtu.be/43gRI03rRWY>`_

`Density guided simulations – combining cryo-EM data and molecular dynamics simulation <https://youtu.be/D8Cu06HVHe0>`_

`A walk through simulation parameter options (.mdp files) for GROMACS <https://youtu.be/0JwSpuysCfc>`_

`Accelerating sampling in GROMACS with the AWH method <https://youtu.be/SN-Ompgp-mE>`_


GROMACS release webinars
------------------------

`GROMACS 2024: new features and improvements <https://youtu.be/5LB6ebxq3ek>`_

`What’s new in GROMACS 2023 <https://youtu.be/3unq1H23aOk>`_

`What’s new in GROMACS 2022 <https://youtu.be/yIblJjez6P0>`_

`What’s new in GROMACS 2021 <https://youtu.be/5WVc7S_yhWw>`_

`What’s new in GROMACS 2020 <https://youtu.be/GKsAkVrIuak>`_
