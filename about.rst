About GROMACS  
==================

GROMACS is a **versatile package to perform molecular dynamics**, i.e. simulate
the Newtonian equations of motion for systems with hundreds to millions of
particles and is a **community-driven project**. Contributions are welcome in many forms, including improvements to
documentation, patches to fix bugs, advice on the forums, bug reports that let us reproduce the issue, and new functionality. See :doc:`\development` page and :doc:`\report_issue`.

It is primarily designed for biochemical molecules like proteins, lipids and
nucleic acids that have a lot of complicated bonded interactions, but since
GROMACS is extremely fast at calculating the nonbonded interactions (that
usually dominate simulations) many groups are also using it for research on
non-biological systems, e.g. polymers and fluid dynamics.

GROMACS supports all the usual algorithms you expect from a modern molecular dynamics implementation, (check the online reference or manual for details), but there are also quite a few features that make it stand out from the competition:

* GROMACS provides extremely high performance compared to all other
  programs. A lot of algorithmic optimizations have been introduced in the
  code; we have for instance extracted the calculation of the virial from the
  innermost loops over pairwise interactions, and we use our own software
  routines to calculate the inverse square root. The compute kernels are
  written using SIMD intrinsics for CPUs and CUDA, OpenCL, and SYCL for GPUs.
  Thus the full potential of all modern CPU and GPUs can be exploited.

* GROMACS can make simultaneous use of both CPU and GPU available in a system.
  There are options to statically and dynamically balance the load between the
  different resources.

* GROMACS is user-friendly, with topologies and parameter files written in clear text format. There is a lot of consistency checking, and clear error messages are issued when something is wrong. Since a C-preprocessor is used, you can have conditional parts in your topologies and include other files. You can even compress most files and GROMACS will automatically pipe them through gzip upon reading.

* There is no scripting required - all programs use a simple interface with
  command line options for input and output files. You can always get help on
  the options by using the -h option, or use the extensive manuals provided
  free of charge in electronic or paper format. There is ongoing work on a
  Python API which enables scripting of simulation setup, running and analysis.

* Both run input files and trajectories are independent of hardware endian-ness, and can thus be read by any version GROMACS, even if it was compiled using a different floating-point precision.

* GROMACS can write coordinates using lossy compression, which provides a very compact way of storing trajectory data. The accuracy can be selected by the user.

* GROMACS comes with a large selection of flexible tools for trajectory
  analysis and the output formats are also supported by all major analysis
  and visualisation packages.

* GROMACS can be run in parallel, using either the standard MPI communication protocol, or via our own "Thread MPI" library for single-node workstations.

* GROMACS contains several state-of-the-art algorithms that make it possible to extend the time steps is simulations significantly, and thereby further enhance performance without sacrificing accuracy or detail.

* The package includes a fully automated topology builder for proteins, even multimeric structures. Building blocks are available for the 20 standard aminoacid residues as well as some modified ones, the 4 nucleotide and 4 deoxinucleotide resides, several sugars and lipids, and some special groups like hemes and several small molecules.

* GROMACS is extensible through with modules through the so-called MDModules
  interface. This currently provides a way for added extra forces and it
  continuously being extended with new functionality.

* GROMACS is a proper name. It is not an acronym for anything.
  
.. include:: docs/license.txt

.. image:: images/GMX_logos/gmx_logo_blue.png
	   

