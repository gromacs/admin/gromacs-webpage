gmxapi Python interface for GROMACS
-----------------------------------

.. image:: ../images/gmxapi-python.png

:py:mod:`gmxapi`
allows molecular simulation and analysis work to be staged and run from Python.

Operations can be connected flexibly to allow high performance simulation and
analysis with complex control and data flows. Users can define new operations
in C++ or Python with the same tool kit used to implement this package.

See http://manual.gromacs.org/current/gmxapi for details on installation and usage.

**Reference**

Irrgang, M. E., Hays, J. M., & Kasson, P. M.
gmxapi: a high-level interface for advanced control and extension of molecular dynamics simulations.
*Bioinformatics* 2018.
DOI: `10.1093/bioinformatics/bty484 <https://doi.org/10.1093/bioinformatics/bty484>`_
