![Build Status](https://gitlab.com/pages/sphinx/badges/master/pipeline.svg)

---

This is the GROMACS webpage source repository.
It is written in [reStructuredText] which is a simple plain-text markup language also used for the GROMACS documentation. Those text files are built with the Python-based [sphinx] documentat ion builder.
It is deloyed automatically as a website to https://gromacs.gitlab.io/admin/gromacs-webpage/ using [GitLab Pages].

Learn more about GitLab Pages at https://about.gitlab.com/product/pages/ and the official
documentation https://docs.gitlab.com/ee/user/project/pages/.
Example [sphinx] documentation website using GitLab Pages.

---

Webpage structure

![Webpage diagram](images/webpage_diagram.png)

`index.rst` page contains a table of contents, and an introduction pointing
to different pages in the table of contents. The parts has to be frequently
updated are included as text (like this `{{text_like_this}}`). The
corresponding files are in docs/

Images used on the website are found in the `images` folder.

The project's static Pages are built by [GitLab CI][ci], following the step
s defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

conf.py (containing Python code) is called the “build configuration file” and
contains (almost) all configuration needed to customize [Sphinx][sphinx] behavior.

Note:

1) rst files in the Documentation dir are needed to resolve the hardcode links to http://www.gromacs.org/Documentation/Floating_Point_Arithmetic or http://www.gromacs.org/Documentation/Errors. Those pages are not in the table of contents (index.rst file) and  redirect to the most recent corresponding documentation page.

2) rst file Downloads.rst is needed to restore http://www.gromacs.org/Downloads.html (links to it are known to exist in GROMACS installations) 

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: python:3.7-alpine

test:
  stage: test
  script:
  - pip install -U sphinx
  - sphinx-build -b html . public
  only:
  - branches
  except:
  - master

pages:
  stage: deploy
  script:
  - pip install -U sphinx
  - sphinx-build -b html . public
  artifacts:
    paths:
    - public
  only:
  - master
```

## Requirements

- [Sphinx][]

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][sphinx] Sphinx
1. Generate the documentation: `make` 

The generated HTML will be located in the location specified by `conf.py`,
in this case `gmxbuild/html`. To clean `gmxbuild/html` run `make clean`.

## Troubleshooting

No issues reported yet.

---

Forked from https://gitlab.com/Eothred/sphinx

[ci]: https://about.gitlab.com/product/continuous-integration/
[sphinx]: http://www.sphinx-doc.org/
[reStructuredText]: https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html
[GitLab Pages]: https://about.gitlab.com/product/pages/

