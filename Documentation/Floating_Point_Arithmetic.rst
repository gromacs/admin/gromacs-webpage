Floating point arithmetic
-------------------------

This document was moved to the GROMACS user manual. Please refer to the `latest version <https://manual.gromacs.org/current/user-guide/floating-point.html>`_.
