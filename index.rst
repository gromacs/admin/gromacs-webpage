.. gromacs-webpage documentation master file, created by
   sphinx-quickstart on Thu Apr 22 17:35:08 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GROMACS
=======================================      
A free and open-source software suite for high-performance molecular dynamics and output analysis.

**New to GROMACS**:

* Try the `introduction tutorial
  <https://tutorials.gromacs.org/md-intro-tutorial.html>`_.
* Watch the `GROMACS webinar <https://www.youtube.com/watch?v=MWafKFVgFTU>`_  
* Download the current GROMACS version `here
  <https://manual.gromacs.org/current/download.html>`_.
* Have a look at `documentation
  <https://manual.gromacs.org/current/index.html>`_ page to know more how to
  install and use GROMACS.
* Do you have any questions, have a look at the user discussions on `GROMACS forums <http://forums.gromacs.org>`_.

.. admonition:: News
  
  "GROMACS 2025.1 is available."  Read here for an overview of the patch
  `release notes <https://manual.gromacs.org/2025.1/release-notes/index.html>`_ - 11 March 2025

  "GROMACS 2025.0 is available." For an overview of the most 
  recent version see the `release highlights <https://manual.gromacs.org/2025.0/release-notes/2025/major/highlights.html>`_ and the  `release notes <https://manual.gromacs.org/2025.0/release-notes/index.html>`_ - 12 Febraury 2025

  Come to listen "What’s new in GROMACS 2025" 18th Febraury 2025 at 15:00 CET. Click `here <https://bioexcel.eu/webinar-whats-new-in-gromacs-2025-2025-2-18/>`_ to register and know more about the webinar speaker. -  10 Febraury 2025

  "GROMACS 2024.5 is available." Read here for an overview of the patch `release notes
  <https://manual.gromacs.org/2024.5/release-notes/2024/2024.5.html>`_ - 30 January 2025

  "GROMACS 2025-beta is available." Please `download it <https://manual.gromacs.org/documentation/2025.0-beta/download.html>`_  and test it - thank you - 20 November 2024

  "GROMACS 2024.4 is available." Read here for an overview of the patch `release notes
  <https://manual.gromacs.org/2024.4/release-notes/2024/2024.4.html>`_ - 1 November 2024

  Workshop Learn to code in GROMACS: Material is available `online <https://zenodo.org/records/13739992>`_ and  
  `the play list here <https://www.youtube.com/playlist?list=PLzLqYW5ci-2d82Pbcv9r712NQlCgjF-2E>`_  - 16 September 2024

  "GROMACS 2024.3 is available." Read here for an overview of the patch `release notes
  <https://manual.gromacs.org/2024.3/release-notes/2024/2024.3.html>`_ - 2 September 2024
  
  `GROMACS User Survey 2024 <https://docs.google.com/forms/d/1uGpw1hnMcUyQWi4sbcmrNZnpI9vjsE-YUdmHS3ocDlQ/viewform?edit_requested=true>`_ is online. This year is the Survey is part of a large survey on computational biomolecular research activities. The survey will help the developers to prioritise future GROMACS developments and it should take no longer than 15 minutes to fill out. - 5 August 2024 

  GROMACS 2024 serie is now available on `conda-forge channel <https://anaconda.org/conda-forge/gromacs>`_ - 13 June 2024
		
  :doc:`workshop`: Learn to code in GROMACS - 10- 12 September 2024 - Online - Registration is open!. - 12 June 2024
	
  "GROMACS 2024.2 is available." Read here for an overview of the patch `release notes
  <https://manual.gromacs.org/2024.2/release-notes/2024/2024.2.html>`_ - 10 May 2024
  
  "GROMACS 2023.5 is available." Read here for an overview of the patch `release notes
  <https://manual.gromacs.org/2023.5/release-notes/2023/2023.5.html>`_ - 5 May 2024
  
  The webinar `Enhanced sampling in collective variable space using the Colvars library in GROMACS <https://youtu.be/-8l1Mt4XpVw>`_
  is now online - 10 April 2024

  The webinar `GROMACS 2024: new features and improvements <https://youtu.be/5LB6ebxq3ek>`_ is now online - 
  14 March 2024

  "GROMACS 2024.1 is available." Read here for an overview of the patch `release notes
  <https://manual.gromacs.org/2024.1/release-notes/2024/2024.1.html>`_ - 28 Febraury 2024

  Come to listen "GROMACS 2024: new features and improvements" 5th March 2024 at 15:00 CET. Click `here <https://bioexcel.eu/webinar-gromacs-2024-new-features-and-improvements-2024-03-05/>`_ to register and know more about the webinar speaker. -  9 Febraury 2024

  Lecture and hand-on materials from the workshop "Running GROMACS efficiently on LUMI workshop" are now `online <https://doi.org/10.5281/zenodo.10683366>`_. The `collaborative document <https://zenodo.org/records/10683366/files/Gromacs-on-LUMI-Q_A-document.pdf?download=1>`_ used during the workshop contains links to lecture and hands-on materials. - 9 Febraury 2024

 	     
.. image:: images/GMX_logos/gmx_logo_blue.png

.. toctree::
   :hidden:
   :maxdepth: 1
	      
   about
   highlights
   Documentation <https://manual.gromacs.org/current/index.html>
   articles
   tutorial_webinar
   Downloads <https://manual.gromacs.org>
   GROMACS forum <http://forum.gromacs.org>
   report_issue 
   development
   user_contributions
   user_survey
   workshop

 
..
.. =======================   
..    Indices and tables
.. =======================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`


